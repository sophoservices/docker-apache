#!/bin/sh

if [ ! -e /var/lib/mysql/mysql ]; then
  mariadb-install-db
  chown -R mysql.mysql /var/lib/mysql
fi

service cron start
service mariadb start
service php7.4-fpm start

if [ -x /var/www/scripts/boot.sh ]; then 
  /var/www/scripts/boot.sh
fi

# tail until we're cancelled
tail -f /var/log/php7.4-fpm.log
