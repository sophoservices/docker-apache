#!/bin/sh

echo "Adding the following options: $*"
docker run -d --restart=always -it \
	-v "$PWD/cron.d/:/etc/cron.d/" \
	-v "$PWD/html/:/var/www/html/" \
	-v "$PWD/db:/var/lib/mysql" \
	-v "$PWD/scripts:/var/www/scripts" \
	--name `basename $PWD` $* php7.4:jammy
