#!/bin/sh

echo "Container $PWD"
echo "Adding the following options: $*"
docker run -d --restart=always -it \
	-v "$PWD/cron.d/:/etc/cron.d/" \
	-v "$PWD/html/:/var/www/html/" \
	-v "$PWD/db:/var/lib/mysql" \
	-v "$PWD/scripts:/var/www/scripts" \
  -v "$PWD/apache:/etc/apache2/conf.d/" \
	--name `basename $PWD` $* apache-php7.4:jammy
