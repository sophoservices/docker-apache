#!/bin/sh

echo "Fetching site configs"
find /var/www -name "*apache.conf" -exec cp {} /etc/apache2/sites-enabled/ \;

# Start Apache
apache2ctl start

# tail until we're cancelled
tail -f /var/log/apache2/access.log &
tail -f /var/log/apache2/error.log
