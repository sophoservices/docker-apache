#!/bin/sh

docker run -d --restart=always -it -p80:80 -p443:443 -v /home/sites/:/var/www/ -v /home/techdave/apache-certs:/etc/letsencrypt --name apache apache:jammy
